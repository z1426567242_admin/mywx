
const app = getApp()
const duration = 2000

Page({
  onShow() {
    wx.reportAnalytics('enter_home_programmatically', {})
  },
  onShareAppMessage() {
    return {
      title: '小程序官方组件展示',
      path: 'page/component/index'
    }
  },

  data: {
    list: [
      {
        id: 'list',
        name: '列表',
        open: false,
        pages: ['list']
      }
    ],
	 
  },

  kindToggle(e) {
    const id = e.currentTarget.id
    const list = this.data.list
    for (let i = 0, len = list.length; i < len; ++i) {
      if (list[i].id === id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list
    })
    wx.reportAnalytics('click_view_programmatically', {})
  },
  
onLoad() {
	
    const userInfo = this.getUserInfo() 
    console.log(userInfo)
    this.setData({
      hasUserInfo: app.globalData.hasUserInfo,
      userInfo : userInfo
    })
	},
  login() {
    const that = this
    wx.login({
      success() {
        app.globalData.hasLogin = true
        that.setData({
          hasLogin: true
        })
      }
    })
  },
  getUserInfo() {
    const that = this
    let userName
    let avatarUrl
    wx.getUserInfo({
        success(info) {
        console.log(info)
        app.globalData.hasUserInfo = true
        serName =   wx.getStorageSync('userName')
        avatarUrl = wx.getStorageSync('avatarUrl')

          if(userName== info.userInfo.nickName  && avatarUrl == info.userInfo.avatarUrl){
            that.setData({
                userInfo: info.userInfo
              })
          }else{
          wx.request({
            url: "http://localhost:8000/api/users/create",
            data: {
              name: info.userInfo.nickName,
              avatarUrl : info.userInfo.avatarUrl
            },
            method: 'POST',
            success(result) {
              if(result.data.flag == 'success'){

                  wx.showToast({
                    title: '请求成功',
                    icon: 'success',
                    mask: true,
                    duration,
                  })
                  that.setData({
                    userInfo: info.userInfo
                  })

                  wx.setStorageSync('userName', info.userInfo.nickName)
                  wx.setStorageSync('avatarUrl', info.userInfo.avatarUrl)
                  console.log('request success', result)
                }else{
                  console.log('request error', result)

              }
              
            },
      
            fail({errMsg}) {
              console.log('request fail', errMsg)
              self.setData({
                loading: false
              })
            }
          })
        }

       
      }
    })
  }
})
